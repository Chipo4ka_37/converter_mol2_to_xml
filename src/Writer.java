package src;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.io.File;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class Writer {

    public static void main(String[] args) throws ParserConfigurationException, IOException, TransformerException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.newDocument();
        Reader read = new Reader(Paths.get("ATP.mol2"));

        HashMap<String,String> Atoms = read.DataOfAtoms;
        HashMap<String,String> Bonds = read.DataOfBonds;
        Integer CountOfA = read.CountOfAtoms;

        // root element
        Element rootElement = doc.createElement("Simulation");
        doc.appendChild(rootElement);
        rootElement.setAttribute("Name","  ");

        // SystemProperties element + description

        Element SystemProperties = doc.createElement("SystemProperties");
        rootElement.appendChild(SystemProperties);


        Element SimulationBoundary = doc.createElement("SimulationBoundary");
        SimulationBoundary.setAttribute("MinimumBoxVolume","2");
        SimulationBoundary.setAttribute("SimulationBox","2 2 2");
        SystemProperties.appendChild(SimulationBoundary);

        Element Thermostat = doc.createElement("Thermostat");
        Thermostat.setAttribute("EquilibriumTemperature","200");
        Thermostat.setAttribute("MaximumTemperature","10000");
        Thermostat.setAttribute("BerendsenCoupling","0.003");
        Thermostat.setAttribute("Type","BerendsenThermostat");
        SystemProperties.appendChild(Thermostat);

        Element Integrator = doc.createElement("Integrator");
        Integrator.setAttribute("TimeStep","0.001");
        Integrator.setAttribute("Type","VelocityVerlet");
        SystemProperties.appendChild(Integrator);

        // Topology

        Element Topology = doc.createElement("Topology");
        rootElement.appendChild(Topology);

        Element Templates = doc.createElement("Templates");
        Topology.appendChild(Templates);

        Element Residue = doc.createElement("Residue");
        Residue.setAttribute("Name","  ");
        Topology.appendChild(Residue);

        //Create Data of Atoms and Bonds

        Element atoms = doc.createElement("Atoms");
        Element bonds = doc.createElement("Bonds");
        Residue.appendChild(atoms);
        Residue.appendChild(bonds);
        Templates.appendChild(Residue);

        for(Map.Entry<String,String> entry: Atoms.entrySet() ){
            Element atom = doc.createElement("Atom");
            atoms.appendChild(atom);
            atom.setAttribute("Element",entry.getValue());
            atom.setAttribute("Position",entry.getKey());
        }
        for(Map.Entry<String,String> entry: Bonds.entrySet() ){
            Element bond = doc.createElement("Bond");
            bonds.appendChild(bond);
            String[] arr = entry.getValue().split(" ");
            bond.setAttribute("A",arr[0]);
            bond.setAttribute("B",arr[1]);
        }

        // Create Base for Data of Type

        Element ForceFields = doc.createElement("ForceFields");
        Element InteractiveGaussianForceField = doc.createElement("InteractiveGaussianForceField");
        InteractiveGaussianForceField.setAttribute("GradientScaleFactor","200");
        ForceFields.appendChild(InteractiveGaussianForceField);

        Element MM3ForceField = doc.createElement("MM3ForceField");
        Element MM3AtomMappings = doc.createElement("MM3AtomMappings");

        HashMap<Integer,Integer> num = new HashMap<>();
        for (int i=0; i< CountOfA; i++){
            num.put(i,i);
        }

        for (Map.Entry<Integer,Integer> entry: num.entrySet()){
            Element MM3AtomMapping = doc.createElement("MM3AtomMapping");
            MM3AtomMappings.appendChild(MM3AtomMapping);
            MM3AtomMapping.setAttribute("AtomPath",entry.getKey().toString());
            MM3AtomMapping.setAttribute("Type"," ");

        }
        MM3ForceField.appendChild(MM3AtomMappings);
        ForceFields.appendChild(MM3ForceField);

        Element LennardJonesForceField = doc.createElement("LennardJonesForceField");
        Element LennardJonesAtomMappings = doc.createElement("LennardJonesAtomMappings");

        for(Map.Entry<Integer,Integer> entry: num.entrySet()){
            Element LennardJonesAtomMapping = doc.createElement("LennardJonesAtomMapping");
            LennardJonesAtomMappings.appendChild(LennardJonesAtomMapping);
            LennardJonesAtomMapping.setAttribute("AtomPath", entry.getKey().toString());
            LennardJonesAtomMapping.setAttribute("MM3Type"," ");
        }

        LennardJonesForceField.appendChild(LennardJonesAtomMappings);
        ForceFields.appendChild(LennardJonesForceField);

        Templates.appendChild(ForceFields);

        //Create end of xml file

        Element Spawners = doc.createElement("Spawners");
        Element Spawner = doc.createElement("Spawner");
        Spawner.setAttribute("Count"," ");
        Spawner.setAttribute("Template","   ");
        Spawner.setAttribute("Name","   ");
        Spawners.appendChild(Spawner);
        Topology.appendChild(Spawners);


        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT,"yes");
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File("output.xml"));
        transformer.transform(source, result);

        // Output to console for testing
        StreamResult consoleResult = new StreamResult(System.out);
        transformer.transform(source, consoleResult);
    }
}
