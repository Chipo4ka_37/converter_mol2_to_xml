package src;

public class Beliviks {
    public static String definition(char[]s1){
        if(s1[0] == 'C'){
            if(s1[1] == 'l')
                return("Chlorine");
            return("Carbone");
        }
        if(s1[0] == 'H'){
            return("Hydrogen");
        }
        if(s1[0] == 'N'){
            return("Nitrogen");
        }
        if(s1[0] == 'O'){
            return("Oxygen");
        }
        if(s1[0] == 'F'){
            return("Fluorine");
        }
        if(s1[0] == 'S'){
            return("Sulfur");
        }
        if(s1[0] == 'P'){
            return("Phosphorus");
        }
        return null;
    }
}
